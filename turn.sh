#!/usr/bin/env bash
  
if [[ "${2}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook turn.yml -i hosts.yml --limit "${1}" --tags "${2}" -vv
elif [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook turn.yml -i hosts.yml --limit "${1}" -vv
else
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook turn.yml -i hosts.yml  -vv
fi

