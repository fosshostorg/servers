[![pipeline status](https://git.coop/fosshostorg/servers/badges/master/pipeline.svg)](https://git.coop/fosshostorg/servers/-/commits/master)

# FOSSHOST Servers

Ansible configuration for [fosshost.org](https://fosshost.org/) servers, see
the [hosts.yml](hosts.yml) file for the list of all the servers.

At the moment all that this repo does is configure Icinga on
`100.node1.net.fosshost.org`.

## Icinga2

TODO: Sort out `/etc/icinga2/features-enabled/api.conf`
